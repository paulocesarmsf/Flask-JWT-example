from flask import Blueprint, request, jsonify
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_refresh_token_required, get_jwt_identity

from app import UserSchema, User, jwt, db

jwt_blueprint = Blueprint('auth_jwt_blueprint', __name__)


@jwt_blueprint.route('/users', methods=['POST'])
def register():
    schema = UserSchema()
    user_schema = schema.load(request.get_json()).data
    user = User(**user_schema)
    user.gen_hash()

    db.session.add(user)
    db.session.commit()

    return jsonify({'message': 'User {} created!'.format(user.email)}), 201


@jwt_blueprint.route('/auth/login', methods=['POST'])
def login():
    user_schema = UserSchema().load(request.get_json()).data
    user = User.query.filter_by(email=user_schema.get('email')).first()

    if user and user.verify_password(user_schema.get('password')):
        access_token = create_access_token(identity=user_schema.get('email'))
        refresh_token = create_refresh_token(identity=user_schema.get('email'))
        return jsonify({
        'access_token': access_token,
        'refresh_token': refresh_token,
        'message': 'Success'
        }), 200

    return jsonify({
    'message': 'Bad credentials'
    }), 401


@jwt_blueprint.route('/auth/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh_token():
    return jsonify({
        'access_token': create_access_token(identity=get_jwt_identity())
    }), 200


@jwt.unauthorized_loader
def missing_jwt(callback):
    return jsonify({
        'message': 'Authorization Header is Missing'
    }), 401
