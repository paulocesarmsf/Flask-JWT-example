from flask import Blueprint, jsonify
from flask_jwt_extended import jwt_required

find_holy_blueprint = Blueprint('find_holy_blueprint', __name__)


@find_holy_blueprint.route('/find_the_holy_grail')
@jwt_required
def the_holy_grail():
    return jsonify({
        'message': 'You have found the holy grail, huray!'
    }), 200
