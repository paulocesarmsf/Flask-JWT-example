from marshmallow import Schema, fields
from passlib.handlers.pbkdf2 import pbkdf2_sha256

from app import db


class UserSchema(Schema):
    email = fields.Email(required=True)
    password = fields.Str(required=True)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True, nullable=False)
    password = db.Column(db.String(256), nullable=False)

    def gen_hash(self):
        self.password = pbkdf2_sha256.hash(self.password)

    def verify_password(self, password):
        return pbkdf2_sha256.verify(password, self.password)
