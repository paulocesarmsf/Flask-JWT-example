from flask import Flask
from flask_jwt_extended import JWTManager
from flask_sqlalchemy import SQLAlchemy

from config import Config

app = Flask(__name__)
jwt = JWTManager(app)
app.config.from_object(Config())
db = SQLAlchemy(app)

from app.models.user import UserSchema, User
from app.blueprints.find_holy_blueprint import find_holy_blueprint
from app.blueprints.auth_jwt_blueprint import jwt_blueprint

app.register_blueprint(jwt_blueprint, url_prefix='/')
app.register_blueprint(find_holy_blueprint, url_prefix='/')
db.create_all()
