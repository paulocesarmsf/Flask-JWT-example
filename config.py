import os


class Config(object):

    FLASK_APP = os.getenv('FLASK_APP', "app")
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI', 'sqlite:///app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv('SQLALCHEMY_TRACK_MODIFICATIONS', False)
    SECRET_KEY = os.getenv('SECRET_KEY', 'my-awesome-jwt-flask-project')
    JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY', 'super-jwt-top-secret-key')
